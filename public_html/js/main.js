

(function($){
    
   "use strict" ;
    
    $(document).ready(function(){
              
      $('.parallax').parallax("50%", 0.1);
        
     // carosel 
     $('.carousel').carousel({
       interval: 5000
     });
        
      // for tab
      
        $('.tabs a').click(function (e) {
         e.preventDefault();
        $(this).tab('show');
        });


       
     // Sticky Menu
    $(".main_header").sticky({
        topSpacing: 0
    });
       
	
            
            // click to scroll up
	
		$('.scrollup').click(function(){
			$("html,body").animate({
				scrollTop:0
			},600);
			return false;
		});
			$(window).on('scroll',function(){
				
				if($(this).scrollTop() >250){
					$('.scrollup').fadeIn();
				}else{
					$('.scrollup').fadeOut();
				}
				
			});

                     
     
          
    });
    
    
})(jQuery);
